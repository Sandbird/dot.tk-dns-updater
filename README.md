# Urgent update
[As noted in this bug](https://bitbucket.org/toastal/dot.tk-dns-updater/issue/5/not-working-anymore), my.dot.tk moved their shindig on over to freenom. I'm not sure if they have dynamic DNS, but if not, I'll likely start over using `Python` (with a few egg dependencies) due to easy of use and better flexibility. If anyone is opposed to this, feel free to message me or post on the bug above.

# Dot.tk DNS Updater
Because Dot.tk domain cannot be updated easily via typical Dynamic DNS methods such as inadyn, this script will update the DNS automatically manually through my.dot.tk. The script would be good in a cron job or hooked to service that checked IP regularly. Dot.tk DNS Updater has been expanded from the original author to accept command line args, configuration files, CNAME record option, and multiple aliases. Some of the urls and checks were updated as well.

## To Install
Download the file into desired directory (I recommend `/usr/local/bin` or somewhere in `/opt`)
#### Via `curl`
```
#!sh
curl -O /usr/local/bin/tkupdater.sh https://bitbucket.org/toastal/dot.tk-dns-updater/raw/master/tkupdater.sh
```
#### Make the script executable
```
#!sh
chmod +x /usr/local/bin/tkupdater.sh
```

- - -

## Requirements
- [cURL](http://curl.haxx.se/download.html)

## Optional
- `dig`

- - -

## How To Use

#### Update: To make this work you have to get your domain name ids from freenom.com
* Go to https://my.freenom.com/clientarea.php?action=domains
* Click Manage Domain on the domain you want to update, and check the address bar on your browser.
* In the end it will say something like id=1111111. That's the id you need to add in tkupdater.sh
Basically you have 2 lists. One is the .conf file that holds the domain names, and then you have to put the domain ids for each of those names in the .sh file, like so:
```
#!shell

    domfind='example1.tk'
    if [[ $alias == $domfind ]]; then
        domain_number=1111111
    fi
```

#### Via command line args
    /usr/local/bin/tkupdater.sh -c -e email@address.com -p password -a example1.tk -a example2.tk -a example3.tk
#### Via conf file
	/usr/local/bin/tkupdater.sh -f /etc/tkupdater.conf
#### Via default conf file at `/etc/tkupdater.conf`
	/usr/local/bin/tkupdater.sh
The default location for the `tkupdater.conf` file is at `/etc/tkupdater.conf`. You can make and modify your settings in the file to be automatically read without the `-f` command line arg or you can use the `-f` command line arg and include a path to a conf file elsewhere on the system. The main advantage to a conf file is a cleaner, easier to run command since the settings are saved and the process on the machine will not show you sensitive information (email and password). [More information on what the conf file should look like](https://bitbucket.org/toastal/dot.tk-dns-updater/src/master/example.conf).


- - -

## Notes

This script was built on and for [Debian](https://www.debian.org/). The biggest issue seems to come along with `mktemp` as part of [GNU coreutils](https://www.gnu.org/software/coreutils/manual/html_node/mktemp-invocation.html) which supports the `--suffix` flag.

Auto renewing domains is in the works, but I missed the response :(
[See the branch >](https://bitbucket.org/toastal/dot.tk-dns-updater/src/auto-renew/tkupdater.sh#cl-231)