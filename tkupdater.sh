#!/usr/bin/env bash
#
# Dot.tk DNS Updater
# by Kyle Kress
#
# Version 1.0.2
#
# Because Dot.tk domain cannot be updated easily via typical
# Dynamic DNS methods such as inadyn, this script will update
# the DNS automatically manually through my.dot.tk. The script
# would be good in a cron job or hooked to service that checked
# IP regularly. Dot.tk DNS Updater has been expanded from the
# original author to accept command line args, configuration
# files, CNAME record option, and multiple alias at once. Some
# of the urls and checks were updated as well.
#
# This script was modified from Benjamin Abendroth's script:
# http://www.beniweb.de/page/dottk.htm
#
# licensed under GPL3
#rm /tmp/*.htm
git_url='https://bitbucket.org/toastal/dot.tk-dns-updater'

usage_text="Usage: $(basename $0) [-c] [-f <config file>] [-u <username>] [-p <password>] [-a <domain alias>]+"

try_text=$(cat <<EOF
Try '$(basename $0) -h' for help.
For more info see: ${git_url}
EOF
)

help_text=$(cat <<EOF
${usage_text}

  -c    use CNAME record type for www.*.tk to *.tk
  -f    config file location
  -e    dot.tk email address (used as username)
  -p    dot.tk password
  -a    domain name with .tk (this option can be repeated to sync
        more than one site to the same IP address)
EOF
)

die() {
	echo "$2"
	rm -f "$cookie" "$result"
	exit $1
}

command_exists() {
    type "$1" &> /dev/null ;
}

addRecord() {
	#addRecord "www.$alias" "$alias" "$current_ip" "$domain_number"
	# $1 = Type [ A | CNAME | MX ]
	# $2 = Hostname
	# $3 = IP Address
	# $4 = ID on the fields (needs to be grep'd)
	#POST+="&dnsaction=modify&domainid=$4&records[0][line]=&records[0][type]=A&records[0][name]=&records[0][ttl]=300&records[0][value]=$3&records[1][line]=&records[1][type]=CNAME&records[1][name]=WWW&records[1][ttl]=300&records[1][value]=$2"
	POST+="https://my.freenom.com/clientarea.php?managedns=$2&domainid=$4&dnsaction=modify&domainid=$4&records[0][line]=&records[0][name]=&records[0][ttl]=14440&records[0][type]=A&records[0][value]=$3&records[1][line]=&records[1][name]=WWW&records[1][ttl]=300&records[1][type]=CNAME&records[1][value]=$2"
	#POST+="&hosttype_$4=$1&hostname_$4=$2&hostcontent_$4=$3"
}

load_conf_file() {
	# $1 = file path

	if [[ $1 ]]; then
		if [[ -x $1 ]]; then
			. $1
			echo $domain_aliases

		else
			echo 'Error: The config file given does not seem to exist.'
			echo 'Attempting to run anyhow...'
			echo
		fi
	fi
}

# If the default conf exists, load it
default_conf='/etc/tkupdater.conf'
if [[ -x $default_conf ]]; then
	. $default_conf
fi

# Parse command line arguments
while getopts 'cf:a:e:p:h' opt; do
	case $opt in
		f)
			file_path=$OPTARG
			load_conf_file $file_path;;
		c)
			use_cname=true;;
		e)
			tk_email=$OPTARG;;
		p)
			tk_password=$OPTARG;;
		a)
			domain_aliases=(${domain_aliases[@]} $OPTARG);;
		h)
			echo ${help_text}
			exit 1;;
		:|\?)
			echo $usage_text
			echo
			echo ${try_text}
			exit 1;;
	esac
done

# Check to see all required variables are set
if [[ ! $domain_aliases || ! $tk_email || ! $tk_password ]]; then
	echo -n 'Error: One or more of the variables are not set correctly. Check '
	echo 'your command line arguments and config files and try again.'
	echo
	echo ${usage_text}
	echo
	echo ${try_text}
	exit 1
fi

echo '┌──────────────┐'
echo '  TKUpdater.sh'
echo '└──────────────┘'

# Make horizontal rules based on the length of the longest domain alias
longest_string=0
for domain in ${domain_aliases[@]}; do
	if [[ ${#domain} -gt $longest_string ]]; then
		longest_string=${#domain}
	fi
done
hr=$(printf '%*s' "$(($longest_string+2))" ' ' | sed "s/\s/-/g")

echo -n '[+] Getting external IP ... '
# Drop in your own IP getter script
if command_exists './getip.sh'; then
	current_ip=$(./getip.sh)
elif command_exists 'dic'; then
	current_ip=$(dig myip.opendns.com @resolver1.opendns.com +short)
else
	current_ip=$(curl --silent http://ipecho.net/plain)
#this last one is in case you have a dyndns name. Just replace the above else, and
#add your dyndns name bellow
#else
#current_ip=$(host sandbird.dyndns.org | grep address | cut -d " " -f 4)
fi


if [[ $? -eq 0 && -n "$current_ip" ]]; then
	echo "done. [$current_ip]"
else
	die 1 "failed."
fi

agent='Mozilla/5.0 (X11; Debian; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0'
cookie=$(mktemp /tmp/XXX.dotTk.htm)

echo -n '[+] Logging in ... '
curl --silent --compressed --tlsv1 --insecure \
 --output  /dev/null --user-agent "$agent" --cookie-jar "$cookie" \
 --referer 'https://my.freenom.com/dologin.php' \
 --data 'username=$tk_email&password=$tk_password' \
 'https://my.freenom.com/dologin.php'

if [[ $? -eq 0 ]]; then
	echo 'done.'
else
	die 1 'failed.'
fi
echo

domainpanel=$(mktemp /tmp/XXX.dotTkDp.htm)
curl --silent --compressed --tlsv1 --insecure \
 --output "$domainpanel" --user-agent "$agent" --cookie "$cookie" \
 'https://my.freenom.com/clientarea.php?action=domains'

for alias in ${domain_aliases[@]}; do
	echo "┌$hr┐"
	echo "  $alias"
	echo "└$hr┘"
	
	# reset the POST var per alias
	POST=""

	echo -n '[+] Getting domain number ... '
	
	domfind='example1.tk'
	if [[ $alias == $domfind ]]; then
		domain_number=1111111
	fi

	domfind='example2.tk'
	if [[ $alias == $domfind ]]; then
		domain_number=2222222
	fi
	
	domfind='example3.tk'
	if [[ $alias == $domfind ]]; then
		domain_number=3333333
	fi
	

	addRecord "www.$alias" "$alias" "$current_ip" "$domain_number"
	#addRecord "A" "$alias" "$current_ip" "$domain_number"
	#if [[ $use_cname ]]; then
	#	addRecord "CNAME" "www.$alias" "$alias" "$domain_number"
	#else
	#	addRecord "A" "www.$alias" "$current_ip" "$domain_number"
	#fi

	echo -n "[+] Submitting form ... "
	result=`mktemp /tmp/XXX.dotTk.htm`
	curl --silent --compressed --tlsv1 --insecure \
	 --output "$result" --user-agent "$agent" --cookie "$cookie" \
	 --referer "https://my.freenom.com/clientarea.php?managedns=$alias&domainid=$domain_number&page=" \
	 --data "$POST" \
	 'https://my.freenom.com/clientarea.php?managedns=$alias&domainid=$domain_number&page='
	 
	#if [[ $? -eq 0 ]] && grep -q "updated succesfully" "$result" ; then
		echo 'Succesfully updated.'
	#else
	#	die 3 "failed."
	#fi
	echo
	
	curl --silent --compressed --tlsv1 --insecure \
	 --output "$result" --user-agent "$agent" --cookie "$cookie" \
	 'https://my.dot.tk/cgi-bin/logout.taloha'
done
rm /tmp/*.htm
echo 'DNS Update Complete.'
